from pathlib import Path
import likelihood_MF_new as Lik
from scipy import optimize

#############################################
#           Data File Path                  #
#############################################
file_name = Path("C:/Users/riens/Desktop/RPEP/New_Data_RPEP/s10/data_submarine_task_10_punishment.csv")

#############################################
#      Initial Parameter values             #
#############################################
alpha = 0.5 # the learning rate
beta = 0.6 # the inverse temperature

"""
 the lower the inverse temperature, the less the probabilities will differ 
 from each other, lower inverse is better to model random participants
"""
parameters = [alpha, beta]

#############################################
#         Optimising the model              #
#############################################

b = (0.00001, 1.0)
inv_temp_b = (0.1, 1000)
bnds = (b, inv_temp_b)

def constraint(parameters): 
    return abs(parameters)

con = {'type': 'ineq', 'fun': constraint}


estim_param = optimize.minimize(Lik.NLL, parameters, args = (file_name,), bounds=bnds, constraints = con, method = "SLSQP")
p = estim_param.x
NLL, gok_values, accuracy, None_count, list_trial, choice_info, VMF_values, left_pos= Lik.NLL(p, file_name, more = True)





            




            