from pathlib import Path
import likelihood_MF_MB_new as Lik
from scipy import optimize
import numpy as np
import matplotlib.pyplot as plt

#############################################
#           Data File Path                  #
#############################################
file_name = Path("C:/Users/riens/Desktop/RPEP/New_Data_RPEP/s10/data_submarine_task_10_punishment.csv")
#file_name = Path("C:/Users/riens/Desktop/last_try/model1.csv")



#############################################
#      Initial Parameter values             #
#############################################
alpha = 0.5 # the learning rate
beta = 0.5 # the inverse temperature
w = 0.5

"""
 the lower the inverse temperature, the less the probabilities will differ 
 from each other, lower inverse is better to model random participants
"""
parameters = [alpha, beta, w]

b = (0, 1.0)
inv_temp_b = (0.1, 1000)
bnds = (b, inv_temp_b, b)

def constraint(parameters): 
    return abs(parameters)

con = {'type': 'ineq', 'fun': constraint}

#############################################
#         Optimising the model              #
#############################################

estim_param = optimize.minimize(Lik.NLL, parameters, args = (file_name,), bounds=bnds, constraints = con, method = "SLSQP")
p = estim_param.x
NLL, accuracy, df, VNET_values, participant_choice, trials = Lik.NLL(p, file_name, more = True)
    

#############################################
#         Grid Search                       #
############################################
alpha_list = np.arange(0.1, 1, 0.1)
alpha_cnt = range(len(alpha_list))

w_list = np.arange(0.1, 1, 0.1)
w_cnt = range(len(w_list))

beta_list = np.arange(0.1, 1, 0.2)
beta_cnt = range(len(beta_list))

grid = np.zeros((len(alpha_list), len(w_list)))

for beta in beta_list: 
    for i in alpha_cnt: 
        for j in w_cnt: 
            parameters = (alpha_list[i], w_list[j], beta)
            grid[i, j] = Lik.NLL(parameters, file_name)
        
            
    #plot
    fig, ax = plt.subplots()
    img = ax.imshow(grid, cmap=plt.cm.rainbow, interpolation='none', extent=[0,1,1,0]) 
    fig.suptitle('Inverse temperature fixed at: ' + str(round(beta,2)))
    plt.xlabel('Weight')
    plt.ylabel('Alpha')
    plt.colorbar(img) 
    plt.show()
            



            




            