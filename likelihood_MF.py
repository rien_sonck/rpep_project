# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 14:36:26 2019

@author: Rien
"""
import numpy as np
import pandas as pd
import sys
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(suppress=True)

def VM(old_value, alpha, outcome):
    """
    :param old_value:            the old model free value
    :param alpha:                the learning rate
    :param outcome:              the decision of the current trial
    :type old_value:             float
    :type alpha:                 float   
    :type outcome:               int
    :return new_value:           updated model-free value
    :rtype new_value:            float
    """
    
    pred_err = outcome - old_value
    new_value = old_value + alpha * pred_err
    
    return new_value

def softMax (s1, s2, inv_temp):
    """
    :param s1:                 value of the left submarine
    :param s2:                 value of the right submarine 
    :type s1, s2:              string
    :return s1_prob:           probability for s1
    :return s2_prob:           probability for s2
    :rtype s1_prob, s2_prob:   float
    
    This function inputs the rewards for a stimulus pair into the softmax function.
    The softmax function squishes everything between 0 and 1, the probability that 
    each of the stimuli belong to the class 'most rewarding'. 
    The softmax output can thus be seen in terms of the confidence of the learning strategy,
    How confident is the model that this stimuli belongs to the 'most rewarding'class. 
    """
    
    s1_prob = np.exp(inv_temp * s1)/(np.exp(inv_temp * s1) + np.exp(inv_temp * s2))
    s2_prob = 1 - s1_prob
    
    return (s1_prob, s2_prob)



def NLL (parameters, file_name, more = False):
    """
    This function calculates the Negative LogLikelihood 
    """
    
    #############################################
    #          Reading in Data                  #
    #############################################
    data = pd.read_csv(file_name)
    nrows = data.shape[0]
    ntrials = 250
    
    #############################################
    #         Initial Variables                 #
    #############################################
    alpha = parameters[0]
    #w = 0.5
    inv_temp = parameters[1]
    participant_choice_prob = np.zeros((ntrials))
    trial_logs = np.zeros((ntrials))
    VMF_values = np.zeros((4,ntrials + 1)) # model-free values for each submarine
    hit = 0.0
    prob_values = np.full((4,ntrials + 1), 0.5) # model's probability for selecting a submarine
    sub_index_dict = {"pink": 0, "blue": 1, "white": 2, "yellow": 3}
    NLL = 0.0
    
    trial = 0
    gok_values = np.zeros((ntrials))
    accuracy = 0.0
    choice_info = np.zeros((8, ntrials))
    choice_info_choice = []
    choice_info_nonchoice = []
    None_count = 0
    list_trial = []
    s1s3_points = np.zeros((ntrials + 1))
    s2s4_points = np.zeros((ntrials + 1))
    
    
        
    #############################################
    #      Calculating NLL                      #
    #############################################
    
    # calculating the NLL, starting from row 1
    # row 0 contains participant age, gender, studies. 
    for row in range(1, nrows):
        choice = data.iloc[row, 5] # Participant response key
        lft_sub = sub_index_dict[data.iloc[row, 9]] # Current trial left submarine, converting to a unique number
        lft_sub_col = data.iloc[row, 9]
        rgt_sub = sub_index_dict[data.iloc[row, 10]] # Current trial right submarine, converting to a unique number
        rgt_sub_col = data.iloc[row, 10]
        s1s3_points[trial] = data.iloc[row, 14] # Current trial s1s3 submarine points
        s2s4_points[trial] = data.iloc[row, 15] # Current trial s2s4 submarine points
        left_pos = data.iloc[row, 11]
        right_pos = data.iloc[row,12]
        list_trial.append(trial)
        
        # assigning left and right points
        
        if left_pos == 's1' or left_pos == 's3': 
            lft_points = s1s3_points[trial]
            rgt_points = s2s4_points[trial]
        elif left_pos == 's2' or left_pos == 's4': 
            lft_points = s2s4_points[trial]
            rgt_points = s1s3_points[trial]
        
        #######################################################
        #                 Participant Choice                 #
        ######################################################
        """
        The choice that the participant makes this trial is based on the experience 
        gained from the previous trials, only after the participant made a choice
        and gets a reward, the model-based, model-free and mixed-values for this trial
        are calculated. Here, we save the participant choice probability according to the Model   
        """
        if choice != "None": 
            if choice == "['f']": 
                participant_choice_prob[trial] = prob_values[lft_sub][trial]
                
                choice_info_choice.append(lft_sub_col) 
                choice_info_nonchoice.append(rgt_sub_col)
                choice_info[0][trial] = round(VMF_values[lft_sub][trial], 3)
                choice_info[1][trial] = round(VMF_values[rgt_sub][trial], 3)
                choice_info[3][trial] = lft_points
                choice_info[4][trial] = rgt_points
                choice_info[6][trial] = round(VMF_values[lft_sub - 2][trial], 3)
                choice_info[7][trial] = round(VMF_values[rgt_sub - 2][trial], 3)
                
                #############################################
                #           Model-free Values               #
                #############################################
                
                VMF_values[lft_sub][trial + 1] = VM(VMF_values[lft_sub][trial], alpha, lft_points)
                VMF_values[rgt_sub][trial + 1] =  VMF_values[rgt_sub][trial]
                 
            elif choice == "['j']": 
                participant_choice_prob[trial] = prob_values[rgt_sub][trial] 
                
                choice_info_choice.append(rgt_sub_col) 
                choice_info_nonchoice.append(lft_sub_col)
                choice_info[0][trial] = round(VMF_values[rgt_sub][trial], 3)
                choice_info[1][trial] = round(VMF_values[lft_sub][trial], 3)
                choice_info[3][trial] = rgt_points
                choice_info[4][trial] = lft_points
                choice_info[7][trial] = round(VMF_values[rgt_sub - 2][trial], 3)
                choice_info[6][trial] = round(VMF_values[lft_sub - 2][trial], 3)
                
                #############################################
                #           Model-free Values               #
                #############################################
                VMF_values[rgt_sub][trial + 1] = VM(VMF_values[rgt_sub][trial], alpha, rgt_points)
                VMF_values[lft_sub][trial + 1] =  VMF_values[lft_sub][trial]
                
                
            if participant_choice_prob[trial] > 0.5: 
                gok_values[trial] = 1
                hit += 1   
            else: 
                gok_values[trial] = 0
                
            
            choice_info[2][trial] = '%f' % (round(participant_choice_prob[trial], 3))
        
                
            #############################################
            #            Trial Log and NLL              #
            #############################################
    
            if participant_choice_prob[trial] != 0:
                trial_logs[trial] = np.log(participant_choice_prob[trial])
                NLL += trial_logs[trial]
                
                choice_info[5][trial] = trial_logs[trial]
            else: 
                choice_info[5][trial] = 0
            
            #############################################
            #           Model-free Values               #
            #############################################
            """
            Updating model-free values for each unique submarine.
            """
            
            VMF_values[lft_sub - 2][trial + 1] =  VMF_values[lft_sub - 2][trial]
            VMF_values[rgt_sub - 2][trial + 1] =  VMF_values[rgt_sub - 2][trial]
            
            
            #############################################
            #              Softmax                      #
            #############################################
            
            # calculating probabilities on the new information 
            lft_prob, rgt_prob = softMax(VMF_values[lft_sub][trial + 1], VMF_values[rgt_sub][trial + 1], inv_temp)
                    
            prob_values[lft_sub][trial + 1] = lft_prob
            prob_values[rgt_sub][trial + 1] = rgt_prob
            prob_values[lft_sub - 2][trial + 1] = prob_values[lft_sub - 2][trial]
            prob_values[rgt_sub - 2][trial + 1] = prob_values[rgt_sub - 2][trial]
            
       
        else:
            choice_info_choice.append("None")
            choice_info_nonchoice.append("None")
            None_count += 1
            for i in range(8): 
                choice_info[i][trial] = 0.0
            
            for i in range(4): 
                prob_values[i][trial + 1] =  prob_values[i][trial]
                VMF_values[i][trial + 1] = VMF_values[i][trial]
            
            
        trial += 1
            
    
    accuracy = hit / (ntrials - None_count) 
    
    if more == True: 
        return -NLL, gok_values, accuracy, None_count, list_trial, choice_info, VMF_values, left_pos
    else:
        return -NLL
         