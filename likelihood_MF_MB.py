# -*- coding: utf-8 -*-
"""
Created on Tue Apr 16 14:36:26 2019

@author: Rien
"""
import numpy as np
import pandas as pd
import sys
np.set_printoptions(threshold=sys.maxsize)
np.set_printoptions(suppress=True)

def VM(old_value, alpha, outcome):
    """
    :param old_value:            the old model free value
    :param alpha:                the learning rate
    :param outcome:              the points gathered of the current trial
    :type old_value:             float
    :type alpha:                 float   
    :type outcome:               int
    :return new_value:           updated value
    :rtype new_value:            float
    """
    
    pred_err = outcome - old_value
    new_value = old_value + alpha * pred_err
    # alpha 1, snel leren. niet boven één op nul te kunnen geraken. 
    
    return new_value

def softMax (s1, s2, inv_temp):
    """
    :param s1:                 value of the left submarine
    :param s2:                 value of the right submarine 
    :type s1, s2:              float
    :return s1_prob:           probability for s1
    :return s2_prob:           probability for s2
    :rtype s1_prob, s2_prob:   float
    
    This function inputs the rewards for a stimulus pair into the softmax function.
    The softmax function squishes everything between 0 and 1, the probability that 
    each of the stimuli belong to the class 'most rewarding'. 
    The softmax output can thus be seen in terms of the confidence of the learning strategy,
    How confident is the model that this stimuli belongs to the 'most rewarding'class. 
    """
    
    
    
    s1_prob = np.exp(inv_temp * s1)/(np.exp(inv_temp * s1) + np.exp(inv_temp * s2))
    s2_prob = 1 - s1_prob
    
    return (s1_prob, s2_prob)



def NLL (parameters, file_name, more = False):
    """
    :param parameters:         A list containing 3 parameters [alpha, inverse temp, weight]
    :param file_name:          Data file location
    :param more:               Decides to return or not return extra variables
    :type parameters:          list
    :type file_name            String
    :type more:                Boolean
    :return -NLL:              Negative Likelihood
    :rtype -NLL:               float
    
    This function calculates the Negative LogLikelihood 
    """
    
    #############################################
    #          Reading in Data                  #
    #############################################
    data = pd.read_csv(file_name)
    
    #############################################
    #         Initial Variables                 #
    #############################################
    nrows = data.shape[0] # amount of rows (trials + additional information) in the datafile
    ntrials = 250 # the amount of trials during the experiment
    alpha = parameters[0] # learning rate
    inv_temp = parameters[1] # inverse temperature
    w = parameters[2] # weight between model-free and model-based 
    NLL = 0.0 # negative loglikelihood
    
    # the probability that the model gives to the choice that the participants makes.
    # low probability means that the model considers the participant's choice to be not likely
    # high probability means that the model considers the participant's choice to be likely
    # The point of this script is to get the model to predict the participant's choice, 
    # trying to get the probability high for each choice that the participants makes. 
    participant_choice_prob = np.zeros((ntrials))
    hit = 0.0 # When the model considers the participant's choice as likely ( > 0.5) then hit + 1
    accuracy = 0.0 # accuracy of the model, how many hits it got / how good it could predict the participant's choice
    
    trial_logs = np.zeros((ntrials)) # log for each trial
    VMF_values = np.zeros((4,ntrials + 1)) # model-free values for each submarine (there are 4 submarines)
    VMB_values = np.zeros((4, ntrials + 1)) # model-based values for each submarine
    VNET_values = np.zeros((4, ntrials + 1)) # mixed-model values for each submarine
    prob_values = np.full((4,ntrials + 1), 0.5) # model's probability for selecting a submarine
    sub_index_dict = {"pink": 0, "blue": 1, "white": 2, "yellow": 3} # translating the submarine string names in the datafile to unique indexes for each submarine
    trial = 0 # keeping track of the current trial
    None_count = 0 # the amount that the participant did not give an answer on the experimental task
    s1s3_points = np.zeros((ntrials + 1)) # collects the points for submarine s1 and s3
    s2s4_points = np.zeros((ntrials + 1)) # collects the points for submarine s2 and s4
    
    # Debug variables, gets all translated into a dataframe at the end of the script 
    lft_sub_list = [] # collects which submarine has been shown on the left position of each trial
    rgt_sub_list = [] # collects which submarine has been shown on the right position of each trial
    choice_list = [] # keeps track of all the choices that the participant makes on each trial
    choice_col = "" # color of the chosen submarine
    nonchoice_col = "" # color of the non-chosen submarine
    choice_index = 0 # the unique index of the chosen submarine
    nonchoice_index = 0 # the unique index of the non-chosen submarine
    choice_points = 0 # points of the choosen submarine
    nonchoice_points = 0 # points of the nonchoosen submarine
    choice_info = np.zeros((18, ntrials)) # Brings together different information about the participant's choice
    nonhit = 0
        
    ###############################
    #      Start of the  Loop     #
    ##############################
    
    # Starting from row 1 since trials start on row 1
    # row 0 contains participant information: age, gender, and studies. 
    for row in range(1, nrows):
        choice = data.iloc[row, 5] # Participant response key
        lft_sub_index = sub_index_dict[data.iloc[row, 9]] # Current trial left submarine, converting to a unique number
        rgt_sub_index = sub_index_dict[data.iloc[row, 10]] # Current trial right submarine, converting to a unique number
        lft_sub_col = data.iloc[row, 9]
        rgt_sub_col = data.iloc[row, 10]
        s1s3_points[trial] = data.iloc[row, 14] # Current trial s1s3 submarine points
        s2s4_points[trial] = data.iloc[row, 15] # Current trial s2s4 submarine points
        left_pos = data.iloc[row, 11]
        right_pos = data.iloc[row,12]
        
        # assigning left and right points
        
        if left_pos == 's1' or left_pos == 's3': 
            lft_points = s1s3_points[trial]
            rgt_points = s2s4_points[trial]
        else: 
            lft_points = s2s4_points[trial]
            rgt_points = s1s3_points[trial]
        
        #######################################################
        #                 Participant Choice                 #
        ######################################################
        """
        The choice that the participant makes this trial is based on the experience 
        gained from the previous trials, only after the participant made a choice
        and gets a reward, the model-based, model-free and mixed-values for this trial
        are calculated. Here, we save the participant choice probability according to the Model   
        """
        if choice != "None": 
            if choice == "['f']": 
                participant_choice_prob[trial] = prob_values[lft_sub_index][trial] # Keeping track of the probability that the model assigns to the participant's choice
                
                choice_index = lft_sub_index # Participant chose the left positioned submarine
                nonchoice_index = rgt_sub_index 
                choice_col = lft_sub_col # color of the chosen submarine
                nonchoice_col = rgt_sub_col
                choice_points = lft_points  # points of the chosen submarine
                nonchoice_points = rgt_points
                
            elif choice == "['j']": 
                participant_choice_prob[trial] = prob_values[rgt_sub_index][trial]
                
                choice_index = rgt_sub_index # Participant chose the right positioned submarine 
                nonchoice_index = lft_sub_index
                choice_col = rgt_sub_col # color of the chosen submarine
                nonchoice_col = lft_sub_col 
                choice_points = rgt_points  # points of the chosen submarine
                nonchoice_points = lft_points
                
            if participant_choice_prob[trial] > 0.5:  # If the model considers this choice to be over 50% likely, then it is a hit
                hit += 1
    
            #############################################
            #            Trial Log and NLL              #
            #############################################
            # calculating the trial log. 
            # When the model considers the choice likely (above 50%) then there will be a lower trial log (less error)
            # When the model considers the choice not likely (below 50%) then there will be a higher trial log (more error) 
            # The more the model is considering the choice likely, the lower the trial log: 0.9 (90%) has lower trial log than 0.6 (60%)
    
            if participant_choice_prob[trial] != 0: # can't calculate log of 0 
                trial_logs[trial] = np.log(participant_choice_prob[trial])  
                NLL += trial_logs[trial]                              
                
                # based on the choice and information of this trial, we can updates the values for the next trial
                # on which the next decision will be based. 
                #############################################
                #           Model-free Values               #
                #############################################
                # only updating the submarine VALUE that has been chosen. 
                # transfering the information from the other three submarines to the next trial. 
                
                # The chosen submarine
                VMF_values[choice_index][trial + 1] = VM(VMF_values[choice_index][trial], alpha, choice_points) # Part of the chosen pair
                # The other three submarines
                VMF_values[choice_index - 2][trial + 1] =  VMF_values[choice_index - 2][trial] # part of the chosen pair
                VMF_values[nonchoice_index][trial + 1] =  VMF_values[nonchoice_index][trial] # part of the nonchosen pair
                VMF_values[nonchoice_index - 2][trial + 1] =  VMF_values[nonchoice_index - 2][trial] # part of the nonchosen pair
                
                #############################################
                #           Model-based Values              #
                #############################################
                # only updating the submarine PAIR that has been chosen. 
                # transfering the information from the other pair to the next trial. 
                
                # The chosen pair, updating both with the same value
                VMB_values[choice_index][trial + 1] = VM(VMB_values[choice_index][trial], alpha, choice_points)
                VMB_values[choice_index - 2][trial + 1] = VMB_values[choice_index][trial + 1]
                # The non-chosen pair
                VMB_values[nonchoice_index][trial + 1] = VMB_values[nonchoice_index][trial]
                VMB_values[nonchoice_index - 2][trial + 1] = VMB_values[nonchoice_index - 2][trial]
                
                #############################################
                #            Mixed-model Values             #
                #############################################
                # only updating the submarine PAIR that has been choosen 
                
                VNET_values[choice_index][trial + 1] = w * VMB_values[choice_index][trial + 1] + (1 - w) * VMF_values[choice_index][trial + 1]
                VNET_values[choice_index - 2][trial + 1] = w * VMB_values[choice_index - 2][trial + 1] + (1 - w) * VMF_values[choice_index - 2][trial + 1]
                
                VNET_values[nonchoice_index][trial + 1] = w * VMB_values[nonchoice_index][trial + 1] + (1 - w) * VMF_values[nonchoice_index][trial + 1]
                VNET_values[nonchoice_index - 2][trial + 1] = w * VMB_values[nonchoice_index - 2][trial + 1] + (1 - w) * VMF_values[nonchoice_index - 2][trial + 1]
                
            #############################################
            #              Softmax                      #
            #############################################
            # calculating probabilities on the new values for each submarine
            lft_prob, rgt_prob = softMax(VNET_values[lft_sub_index][trial + 1], VNET_values[rgt_sub_index][trial + 1], inv_temp)
            
            prob_values[lft_sub_index][trial + 1] = lft_prob
            prob_values[rgt_sub_index][trial + 1] = rgt_prob
            prob_values[lft_sub_index - 2][trial + 1] = prob_values[lft_sub_index - 2][trial]
            prob_values[rgt_sub_index - 2][trial + 1] = prob_values[rgt_sub_index - 2][trial]
                        
                
             #############################################
            #    Collecting Debuging Information         #
            #############################################
            
            for i in range(4): 
                choice_info[i][trial] = VMF_values[i][trial]
                choice_info[i + 4][trial] = VMB_values[i][trial]
                choice_info[i + 8][trial] = VNET_values[i][trial]

            lft_sub_list.append(lft_sub_col)
            rgt_sub_list.append(rgt_sub_col)
            choice_info[12][trial] = VNET_values[choice_index][trial]
            choice_info[14][trial] = VNET_values[nonchoice_index][trial]
            choice_list.append(choice_col) 
            choice_info[13][trial] = choice_points
            choice_info[15][trial] = nonchoice_points
            choice_info[16][trial] = '%f' % (round(participant_choice_prob[trial], 3))
            if participant_choice_prob[trial] != 0:
                choice_info[17][trial] = trial_logs[trial]
            else:
                choice_info[17][trial] = 0
                
            
       # when particpant choice equals None
        else:
            choice_list.append("None")
            lft_sub_list.append("None")
            rgt_sub_list.append("None")
            None_count += 1
            
            for i in range(10): 
                choice_info[i][trial] = 0.0
            
            for i in range(4): 
                prob_values[i][trial + 1] =  prob_values[i][trial]
                VMF_values[i][trial + 1] = VMF_values[i][trial]
            
            
        trial += 1
    
    ###########################
    #     End of the Loop     #
    ###########################
            
    
    accuracy = hit / (ntrials - None_count) # Calculating the accuracy of the model
    
    
    df = pd.DataFrame({ 'A- Pink (s1) VMF' : choice_info[0], # Creating a dataframe with all the necessary information to debug the code and check values. 
                        'B- Blue (s2) VMF' : choice_info[1],
                        'C- White (s3) VMF': choice_info[2],
                        'D- Yellow (s4) VMF' : choice_info[3],
                        'E- Pink (s1) VMB' : choice_info[4], 
                        'F- Blue (s2) VMB' : choice_info[5],
                        'G- White (s3) VMB': choice_info[6],
                        'H- Yellow (s4) VMB' : choice_info[7],
                        'I- Pink (s1) VNET' : choice_info[8], 
                        'J- Blue (s2) VNET' : choice_info[9],
                        'K- White (s3) VNET': choice_info[10],
                        'L- Yellow (s4) VNET' : choice_info[11],
                        'M- Left stimuli': lft_sub_list,
                        'N- Right stimuli': rgt_sub_list,
                        'O- Choice' : choice_list,
                        'P- Choice Value' : choice_info[12],
                        'Q- Choice Points' : choice_info[13], 
                        'R- Other Value' : choice_info[14], 
                        'S- Other Points' : choice_info[15],
                        'T- Prob' : choice_info[16],
                        'U- Trial log' : choice_info[17]})
    
    if more == True: # Returns additional information 
        return -NLL, accuracy, df, VNET_values, participant_choice_prob, ntrials - None_count
    else: # Only returns the NLL, used to give to the minimalisation function
        return -NLL
